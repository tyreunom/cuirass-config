(use-modules (guix inferior) (guix channels) (guix store) (guix ui)
             (guix derivations) (guix grafts) (srfi srfi-1) (srfi srfi-26))

(setvbuf (current-error-port) 'line)
(set-current-output-port (current-error-port))

(define (all-packages inferior store system)
  (define proc
    `(lambda (store)
       (define (package->alist package)
         (parameterize ((%graft? #f))
           `((#:job-name . ,(package-full-name package "-"))
             (#:derivation . ,(derivation-file-name (package-derivation store package ,system)))
             (#:description . ,(package-synopsis package))
             (#:long-description . ,(package-description package))
             (#:home-page . ,(package-home-page package))
             (#:maintainers . ("bug-guix@gnu.org"))
             (#:max-silent-time . ,(or (assoc-ref (package-properties package)
                                                  'max-silent-time)
                                       3600))
             (#:timeout . ,(or (assoc-ref (package-properties package)
                                          'timeout)
                               72000)))))
       (map package->alist (fold-packages cons '()))))
  (inferior-eval-with-store inferior store proc))

(define (cuirass-jobs store arguments)
  (define channel
    (assoc-ref arguments 'guix))
  (define instance
    (checkout->channel-instance
      (assq-ref channel 'file-name)
      #:commit (assq-ref channel 'revision)))
  (define derivation
    (run-with-store store
      (channel-instances->derivation (list instance))))
  
  (show-what-to-build store (list derivation))
  (build-derivations store (list derivation))
  
  (format (current-error-port) "args: ~a~%" arguments)
  
  (let ((inferior (open-inferior (derivation->output-path derivation))))
    (inferior-eval '(use-modules (srfi srfi-1) (srfi srfi-26) (guix grafts)) inferior)
    
    (append-map
      (lambda (system)
        (map
          (lambda (spec) (lambda _ spec))
          (all-packages inferior store system)))
      (assoc-ref arguments 'systems))))